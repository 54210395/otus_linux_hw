#!/bin/bash
rm /opt/temporary_file 2> /dev/null
list=`find /proc/ -maxdepth 1 -type d -name '[0-9]*' | awk -F"/" '{print $3}'`
echo "PID STAT TTY COMMAND" | column -t -s ' ' >> /opt/temporary_file
for i in $list
do  
    stat=$(sudo cat /proc/$i/stat 2> /dev/null)
    
    pid=$(echo $stat | awk '{print $1}')
    

done

