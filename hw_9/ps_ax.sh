#!/bin/bash
rm /opt/temporary_file 2> /dev/null
list=`find /proc/ -maxdepth 1 -type d -name '[0-9]*' | awk -F"/" '{print $3}'`
echo "PID STAT TTY COMMAND" | column -t -s ' ' >> /opt/temporary_file
for i in $list
do
    pid=`cat /proc/$i/stat 2> /dev/null | awk '{print $1}'`
    stat=`cat /proc/$i/stat 2> /dev/null | awk '{print $3}'`
    tty=`ls -la /proc/$i/fd/ 2> /dev/null | grep dev | awk '{print $11}' | cut -f3,4 -d"/" | uniq | grep -e pts -e tty`
    if [ -z "${tty}" ]; then
        tty2="?"
    else
        tty2=$tty
    fi

    if [ -f /proc/$i/stat ]; then
    	time=`cat /proc/$i/stat | rev | awk '{print $36" "$37" "$38" "$39}' | rev | awk '{sum=$1+$2+$3+$4}END{print sum/100}' | awk '{("date +%M:%S -d @"$1)| getline $1}1'`
    else
    	time=`echo 'n/a'`
    fi

    comand2=`cat /proc/$i/cmdline 2> /dev/null`
    comand=`cat /proc/$i/comm 2> /dev/null`
    #Не заработало, файлы в fsproc размером в 0 байт, т.к. измерять их размер - дорого.
    #if [[ -s /proc/$i/cmdline ]]; then
    #   comand=`cat /proc/$i/cmdline 2> /dev/null` 
    #else
    #   comand=`cat /proc/$i/comm 2> /dev/null`
    #fi

    echo $pid $stat $tty2 $time [$comand]$comand2 >> /opt/temporary_file

done

#column -t -T -s ' ' /opt/temporary_file 
cat /opt/temporary_file | column -t -s ' ' | cut -c1-$(stty size </dev/tty | cut -d' ' -f2)