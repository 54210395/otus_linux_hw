# Vagrant DNS Lab

  * добавить еще один сервер client2
  * завести в зоне dns.lab имена:
      * web1 - смотрит на клиент1
      * web2 смотрит на клиент2
  * завести еще одну зону newdns.lab
  * завести в ней запись
  * www - смотрит на обоих клиентов
  * настроить split-dns
    *  клиент1 - видит обе зоны, но в зоне dns.lab только web1
    * клиент2 видит только dns.lab

  *настроить все без выключения selinux Формат сдачи ДЗ - vagrant + ansible


---

Для запуска стенда воспользуйтесь командой:
```
vagrant up
```

Для проверки работы split-dns проверьте адрес ```www.newdns.lab``` на ```client``` и ```client2```

```
vagrant ssh client
[vagrant@client ~]$ dig +short www.newdns.lab
```

```
vagrant ssh client2
[vagrant@client2 ~]$ dig +short www.newdns.lab
```