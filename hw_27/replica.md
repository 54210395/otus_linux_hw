ssh your_ip
hostname
ss -ntl
sudo -u postgres psql
\l
sudo pg_ctlcluster 12 main stop
sudo rm -rf /var/lib/postgresql/12/main/
sudo -u postgres pg_basebackup -h your_ip -R -D /var/lib/postgresql/12/main -U postgres -W
sudo pg_ctlcluster 12 main start

sudo -u postgres psql
\l
\c replica
select * from t;
select pg_is_in_recovery();

-- логическая репликация
sudo pg_ctlcluster 12 main promote
sudo pg_ctlcluster 12 main restart;
select * from t;
--создадим подписку на втором экземпляре
--CREATE TABLE test(i int);-- Проверить
--создадим подписку к БД по Порту с Юзером и Паролем 
CREATE SUBSCRIPTION test_sub 
CONNECTION 'host=your_ip port=5432 user=postgres password=Pa$$w0rd dbname=replica' 
PUBLICATION test_pub ;
-- WITH (copy_data = false); --и Копированием данных=false

select * from pg_replication_slots;
select * from pg_subscription;
-- проверим, что данные передаются
select * from test;
SELECT * FROM pg_stat_subscription;
drop subscription test_sub;