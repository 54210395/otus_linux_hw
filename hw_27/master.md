ubuntu - команда для ubuntu
(rhel) - команда для rhel
=========
hostname
ip addr show

Посмотрим в каком режиме работает данный кластер:
```
postgres=# select pg_is_in_recovery();
 pg_is_in_recovery 
-------------------
 f
(1 row)
```
*f - (fast) он не работает в режиме востановления, является мастером.

Посмотрим есть ли записи в pg_stat_replication:
```
postgres=# select * from pg_stat_replication;
 pid | usesysid | usename | application_name | client_addr | client_hostname |
 client_port | backend_start | backend_xmin | state | sent_lsn | write_lsn | f
lush_lsn | replay_lsn | write_lag | flush_lag | replay_lag | sync_priority | s
ync_state | reply_time 
-----+----------+---------+------------------+-------------+-----------------+
-------------+---------------+--------------+-------+----------+-----------+--
---------+------------+-----------+-----------+------------+---------------+--
----------+------------
(0 rows)
```

Сосздадим БД для репликации:
```
postgres=# create database replica;
CREATE DATABASE
```


sudo nano /etc/postgresql/12/main/postgresql.conf 
(sudo nano /var/lib/pgsql/14/data/postgresql.conf)
-- мастер должен слушать реплику
-- echo 'listen_address = 'localhost, IP_REPLIC' ' >> /etc/postgresql/12/main/postgresql.conf
(-- echo 'listen_address = 'localhost, IP_REPLIC' ' >> /var/lib/pgsql/14/data/postgresql.conf)
-- Задает TCP / IP-адреса, на которых сервер должен прослушивать соединения от клиентских приложений. 
sudo nano /etc/postgresql/12/main/pg_hba.conf  
(IPv4 local connections)
-- мастер должен разрешить доступные IP интерфейсы
-- echo 'host all all 0.0.0.0/0 md5' >> /etc/postgresql/12/main/pg_hba.conf != 127.0.0.1/32
(host all all 192.168.50.11/32 md5)
-- разрешает входящие соединения на всех доступных IP-интерфейсах.
-- если вы хотите, чтобы был авторизован некоторый диапазон IP-адресов,
-- 
-- echo 'host relication all 0.0.0.0/0 md5' >> /etc/postgresql/12/main/pg_hba.conf != 127.0.0.1/32
(host replication all 192.168.50.11/32 md5)

ss -ntl

sudo -u postgres psql
select pg_is_in_recovery();
select * from pg_stat_replication;
\l
\du

sudo pg_ctlcluster 12 main restart; 

sudo -u postgres psql
select * from pg_stat_replication;
select pg_is_in_recovery();
CREATE DATABASE replica;
\c replica
create table t (t int);
insert into t values(0);
select * from t;
-- \dt 

insert into t values(1);

--логическая репликация
--wal_level= logical
sudo nano /etc/postgresql/12/main/postgresql.conf  
--ALTER SYSTEM SET wal_level = logical;
sudo pg_ctlcluster 12 main restart;

CREATE TABLE test(i int);
-- создать публикацию
CREATE PUBLICATION test_pub FOR TABLE test;
--посмотрим что получилось
select * from pg_publication;
select * from pg_publication_tables;
--проверим, что можно переносить и данные
insert into test values(1);
select * from test;
drop publication test_pub;
--#wal_level= replica
sudo nano /etc/postgresql/12/main/postgresql.conf 

