Репликация MySQL
===
CentOS-7; Percona 5.7.


![](./schem.png)


## Устанавливаем Percona 5.7
#Репозиторий
```
yum install https://repo.percona.com/yum/percona-release-latest.noarch.rpm -y
```
#СерверMySQL
```
yum install Percona-Server-server-57 -y
```
Копируем конфиги из /vagrant/conf.d в /etc/my.cnf.d/
```
cp /vagrant/conf/conf.d/* /etc/my.cnf.d/
```
Запускаем службу mysql
```
systemctl start mysql
```
Узнаем пароль от рута
```
cat /var/log/mysqld.log | grep 'root@localhost:' | awk '{print $11}'
```
Входим в БД под рутом, с паролем из предыдущего шага
```
mysql -uroot -p'dWietQlVz8!v'
```
Меняем пароль руту (без этого шага нас не пустят далее)
```sql
mysql> ALTER USER USER() IDENTIFIED BY 'hai4miva3E';
```

## Репликацию будем настраивать с использованием GTID.

> атрибут server-id на мастер-сервере должен обязательно
отличаться от server-id слейв-сервера

Посмотреть id
```
mysql> SELECT @@server_id;
+-------------+
| @@server_id |
+-------------+
|           1 |
+-------------+
1 row in set (0.00 sec)
```

Убеждаемся что GTID включен:
```
mysql> SHOW VARIABLES LIKE 'gtid_mode';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| gtid_mode     | ON    |
+---------------+-------+
1 row in set (0.00 sec)

```

## Создадим тестовую базу bet и загрузим в нее дамп и проверим:
Создадим
```
mysql> CREATE DATABASE bet;
Query OK, 1 row affected (0.00 sec)
```
Загрузим дамп
```
[root@master ~]# mysql -uroot -p -D bet < /vagrant/bet.dmp
```
Проверим
```
mysql> USE bet;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> SHOW TABLES;
+------------------+
| Tables_in_bet    |
+------------------+
| bookmaker        |
| competition      |
| events_on_demand |
| market           |
| odds             |
| outcome          |
| v_same_event     |
+------------------+
7 rows in set (0.00 sec)
```

Создадим пользователя для репликации
```
mysql> CREATE USER 'repl'@'%' IDENTIFIED BY '!OtusLinux2018';
Query OK, 0 rows affected (0.01 sec)
```
Проверим
```
mysql> SELECT user,host FROM mysql.user where user='repl';
+------+------+
| user | host |
+------+------+
| repl | %    |
+------+------+
1 row in set (0.00 sec)
```

Дадим пользователю права на репликацию:
```
mysql> GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%' IDENTIFIED BY '!OtusLinux2018';
Query OK, 0 rows affected, 1 warning (0.01 sec)
```

Перечитаем привелегии:
```
mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.01 sec)
```

## Дампим базу для последующего залива на слэйв и игнорируем таблицы по заданию:
```
[root@master ~]# mysqldump --all-databases --triggers --routines --master-data --ignore-table=bet.events_on_demand --ignore-table=bet.v_same_event -uroot -p > master.sql
```

На этом настройка Master-а завершена. Файл дампа нужно залить на слейв

```
scp master.sql 192.168.11.151:~/

root@192.168.11.151's password: vagrant
```  

---

## Переходим к slave-серверу

Устанавливаем percona так же как на master


Так же копируем конфиги из /vagrant/conf.d в /etc/my.cnf.d/
```
[root@slave ~]# cp /vagrant/conf/conf.d/* /etc/my.cnf.d/
```

Правим в /etc/my.cnf.d/01-base.cnf директиву ```server-id = 2```  


Раскомментируем в /etc/my.cnf.d/05-binlog.cnf строки:
```
#replicate-ignore-table=bet.events_on_demand
#replicate-ignore-table=bet.v_same_event
```

Запускаем мускуль:
```
[root@slave ~]# systemctl start mysql
```

Так же как и на master, ищем в логах пароль от root, заходим под ним и меняем пароль.

Проверяем на сервере, какой текущий server-id
```
mysql> SELECT @@server_id;
+-------------+
| @@server_id |
+-------------+
|           2 |
+-------------+
1 row in set (0.00 sec)
```

Заливаем дамп мастера и убеждаемся что база есть и она без лишних таблиц:
```
mysql> SOURCE /root/master.sql
```

При загрузке дампа словил ошибку:
```
ERROR 1840 (HY000): @@GLOBAL.GTID_PURGED can only be set when @@GLOBAL.GTID_EXECUTED is empty.
```
что не позволило мне запустить slave (start slave)

---

<details>
 <summary> Решение ошибки 1840 </summary>
Суть проблемы в том что при запуске на слейве: 

```
mysql> show global variables like 'GTID_EXECUTED';
+---------------+------------------------------------------+
| Variable_name | Value                                    |
+---------------+------------------------------------------+
| gtid_executed | 116815f8-1332-11ed-8ef8-5254004d77d3:1-2 |
+---------------+------------------------------------------+
1 row in set (0.00 sec)

```

а на местере:

```
mysql> show global variables like 'GTID_EXECUTED';
+---------------+-------------------------------------------+
| Variable_name | Value                                     |
+---------------+-------------------------------------------+
| gtid_executed | c6109764-1329-11ed-8928-5254004d77d3:1-39 |
+---------------+-------------------------------------------+
1 row in set (0.00 sec)
```

и ```Slave_SQL_Running: No```

Решается это следующим образом:
На слейве:
Останавливаем репликацию

```
mysql> stop slave;
Query OK, 0 rows affected (0.01 sec)
```

```
mysql> reset master;
Query OK, 0 rows affected (0.03 sec)
```

```
mysql> show global variables like 'GTID_EXECUTED';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| gtid_executed |       |
+---------------+-------+
1 row in set (0.00 sec)

```

Копируем с мастера указатель последней транзакции и прописываем его на слейве:

```
mysql> set global GTID_PURGED = "c6109764-1329-11ed-8928-5254004d77d3:1-39";
Query OK, 0 rows affected (0.01 sec)
```

```
mysql> start slave;
Query OK, 0 rows affected (0.03 sec)
```

Проверяем статус репликации:
```
mysql> show slave status \G;
```

И тут параметр ```Slave_SQL_Running: Yes``` должен быть со значением Yes. Это значит слейв работает

</details>  

---

```
mysql> SHOW DATABASES LIKE 'bet';
+----------------+
| Database (bet) |
+----------------+
| bet            |
+----------------+
1 row in set (0.00 sec)
```
```
mysql> USE bet;
```
```
mysql>  SHOW TABLES;
+---------------+
| Tables_in_bet |
+---------------+
| bookmaker     |
| competition   |
| market        |
| odds          |
| outcome       |
+---------------+
5 rows in set (0.00 sec)
```

подключаем и запускаем слейв:
```
mysql> CHANGE MASTER TO MASTER_HOST = "192.168.11.150", MASTER_PORT = 3306, MASTER_USER = "repl", MASTER_PASSWORD = "!OtusLinux2018", MASTER_AUTO_POSITION = 1;
Query OK, 0 rows affected, 2 warnings (0.08 sec)
```
```
mysql>  START SLAVE;
Query OK, 0 rows affected (0.04 sec)
```

```
mysql> show slave status \G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.11.150
                  Master_User: repl
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 154
               Relay_Log_File: slave-relay-bin.000004
                Relay_Log_Pos: 407
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: bet.events_on_demand,bet.v_same_event
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 154
              Relay_Log_Space: 914
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: c6109764-1329-11ed-8928-5254004d77d3
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: c6109764-1329-11ed-8928-5254004d77d3:1-39
            Executed_Gtid_Set: c6109764-1329-11ed-8928-5254004d77d3:1-39
                Auto_Position: 1
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)

ERROR: 
No query specified
```

> обращаем внимение на состояние ```Slave_SQL_Running```

## Проверим репликацию в действии. 
На мастере:

```
mysql> USE bet;
Database changed
mysql> INSERT INTO bookmaker (id,bookmaker_name) VALUES(1,'1xbet');
Query OK, 1 row affected (0.01 sec)

mysql> SELECT * FROM bookmaker;
+----+----------------+
| id | bookmaker_name |
+----+----------------+
|  1 | 1xbet          |
|  4 | betway         |
|  5 | bwin           |
|  6 | ladbrokes      |
|  3 | unibet         |
+----+----------------+
5 rows in set (0.00 sec)
```

На слейве:

```
mysql> SELECT * FROM bookmaker;
+----+----------------+
| id | bookmaker_name |
+----+----------------+
|  1 | 1xbet          |
|  4 | betway         |
|  5 | bwin           |
|  6 | ladbrokes      |
|  3 | unibet         |
+----+----------------+
5 rows in set (0.00 sec)
```

