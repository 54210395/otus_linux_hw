
### **Домашнее задание(hw)/проектная работа разработано(-на) для курса "[Administrator Linux. Professional](https://otus.ru/lessons/linux-professional/?int_source=courses_catalog&int_term=operations?utm_source=github&utm_medium=free&utm_campaign=otus)"**
# Инструкции

* [Как начать Git](git_quick_start.md)
* [Как начать Vagrant](vagrant_quick_start.md)

## otus-linux

Используйте этот [Vagrantfile](Vagrantfile) - для тестового стенда.

* [Home work 1 - Kernel.](./hw_1)(+)
* [Home work 2 - RAID's.](./hw_2)(+)
* [Home work 3 - LVM.](./hw_3)(+)
* [Home work 4 - ZFS.](./hw_4)(+)
* [Home work 5 - NFS, FUSE.](./hw_5)(+)
* [Home work 6 - Repositories, RPM.](./hw_6)(+)
* [Home work 7 - System load (init).](./hw_7)(+)
* [Home work 8 - Bash-shell, scripting.](./hw_8)(+)
* [Home work 9 - Processes.](./hw_9)(-)
* [Home work 10 - Ansible.](./hw_10)(+)
* [Home work 11 - SELinux.](./hw_11)(+)
* [Home work 12 - Systemd.](./hw_12)(-)
* [Home work 13 - Docker.](./hw_13)(+)
* [Home work 14 - Мониторинг.](./hw_14)(+)
* [Home work 15 - PAM.](./hw_15)(+)
* [Home work 16 - Сбор логов.](./hw_16)(+)
* [Home work 17 - Архитектура сетей.](./hw_17)(-)
* [Home work 18 - Бэкапы.](./hw_18)(-)
* [Home work 19 - DHCP, PXE.](./hw_19)(-)
* [Home work 20 - Сценарии iptables.](./hw_20)(-)
* [Home work 21 - OSPF.](./hw_21)(-)
* [Home work 22 - Мосты, туннели и VPN.](./hw_22)(+)
* [Home work 23 - DNS, split-dns](./hw_23)(+)
* [Home work 24 - VLAN's LACP](./hw_24)(-)
* [Home work 25 - LDAP, FreeIPA](./hw_25)(-)
* [Home work 26 - Развертывание веб приложения](./hw_26)(-)
* [Home work 27 - Репликация postgres](./hw_27)(-)
* [Home work 28 - Репликация mysql](./hw_28)(+)
* [Итоговый проект](./my-project)(+)