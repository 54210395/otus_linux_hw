### node1 (192.168.50.10)

/etc/mysql/mysql.conf.d/mysqld.cnf
```
[mysqld]
user = mysql
bind-address = 0.0.0.0
mysqlx-bind-address = 127.0.0.1
key_buffer_size = 16M
myisam-recover-options = BACKUP
log_error = /var/log/mysql/error.log
server-id = 1
log_bin = /var/log/mysql/mysql-bin.log
max_binlog_size = 100M
```

```systemctl restart mysql```

mysql
CREATE USER 'repl'@'192.168.50.11' IDENTIFIED BY 'SuperP@S$w0rd';
GRANT REPLICATION SLAVE ON *.* TO 'repl'@'192.168.50.11';

SHOW MASTER STATUS \G;

### node2 (192.168.50.11)

/etc/mysql/mysql.conf.d/mysqld.cnf
```
[mysqld]
user = mysql
bind-address = 0.0.0.0
mysqlx-bind-address = 127.0.0.1
key_buffer_size = 16M
myisam-recover-options = BACKUP
log_error = /var/log/mysql/error.log
server-id = 2
log_bin = /var/log/mysql/mysql-bin.log
max_binlog_size = 100M
```

```systemctl restart mysql```

mysql
```
CHANGE REPLICATION SOURCE TO SOURCE_HOST='192.168.50.10',
    SOURCE_LOG_FILE='mysql-bin.000001',
    SOURCE_LOG_POS=946,
    SOURCE_SSL=1;
```

```
START REPLICA USER='repl' PASSWORD='SuperP@S$w0rd';
SHOW REPLICA STATUS \G;
```