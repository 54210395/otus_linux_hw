# Итоговый проект

Требования:

* включен https;
* основная инфраструктура в DMZ зоне;
* файрвалл на входе;
* сбор метрик и настроенный алертинг;
* везде включен selinux;
* организован централизованный сбор логов;
* организован backup.

Схема проекта:

![](./scheme.png)


просле прохождения playbook.yml 

Настроим репликацию master-slave руками:

### node1 (192.168.50.10)
Зайдем в mysql из под рута:
```
[root@web ~]# mysql
```
Создадим в ней пользователя для репликации с ограничением доступа по ip:
```
mysql> CREATE USER 'repl'@'192.168.50.11' IDENTIFIED BY 'SuperP@S$w0rd';
mysql> GRANT REPLICATION SLAVE ON *.* TO 'repl'@'192.168.50.11';
```
Проверим статус мастера, он нам потребуется для настройки репликации на slave-ноде:
```
mysql> SHOW MASTER STATUS \G;
*************************** 1. row ***************************
             File: mysql-bin.000001    <-нам потребуется эта переменная
         Position: 157                 <-и эта
     Binlog_Do_DB: 
 Binlog_Ignore_DB: 
Executed_Gtid_Set: 
1 row in set (0.00 sec)

ERROR: 
No query specified
```
### node2 (192.168.50.11)
Зайдем в mysql из под рута:
```
[root@web ~]# mysql
```

```
mysql>  CHANGE REPLICATION SOURCE TO SOURCE_HOST='192.168.50.10',
        SOURCE_LOG_FILE='mysql-bin.000001',     <-вставляем сюда первую переменную
        SOURCE_LOG_POS=946,                     <-сюда вторую
        SOURCE_SSL=1;
```
Запустим репликацию, указав учетные данные пользователя ```repl``` :
```
mysql> START REPLICA USER='repl' PASSWORD='SuperP@S$w0rd';
mysql> SHOW REPLICA STATUS \G;
```
*тут следует проверить параметр ```Last_IO_Error```, он должен быть пуст.


После этого заходим на 192.168.50.10 - для нстройки wp.